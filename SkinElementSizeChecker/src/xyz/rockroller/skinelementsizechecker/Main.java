package xyz.rockroller.skinelementsizechecker;

import java.awt.Dimension;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;

public class Main{

	static List<Path> skinFolders = new ArrayList<Path>();

	// this count is based on skin.ini files and files that end with .osk/.zip
	static int skinCount = 0;

	static String[] inputs = new String[]{
	    // I can just iterate over the entire "E:\\CreativeCommunity\\Skinning - skins", as the osuskinner mass dl needs specific handling
		// @formatter:off
		"C:\\osu!\\Skins",
		"C:\\osu!\\Skins Contest I",
		"C:\\osu!\\Skins Contest II",
		"C:\\osu!\\Skins Contest III",
		"D:\\Downloads\\skins",
		"E:\\CreativeCommunity\\Skinning - skins\\circle people",
		"E:\\CreativeCommunity\\Skinning - skins\\completed subforum",
		"E:\\CreativeCommunity\\Skinning - skins\\extracted",
		"E:\\CreativeCommunity\\Skinning - skins\\my skins",
		"E:\\CreativeCommunity\\Skinning - skins\\osk",
		"E:\\CreativeCommunity\\Skinning - skins\\skins contests"
		// @formatter:on
	};

	static String[] nonAnimatedElements = new String[]{
		// @formatter:off
		"hitcircle",
		"hitcircleoverlay",
		"sliderstartcircle",
		"sliderstartcircleoverlay",
		"sliderendcircle",
		"sliderendcircleoverlay",
		"reversearrow",
		"sliderb-nd",
		"sliderb-spec",
		// only default numbers required
		"default-0",
		"default-1",
		"default-2",
		"default-3",
		"default-4",
		"default-5",
		"default-6",
		"default-7",
		"default-8",
		"default-9",
		"taikohitcircle",
		"taikobigcircle",
		"fruit-bananas",
		"fruit-bananas-overlay",
		"fruit-drop",
		"fruit-drop-overlay",
		"fruit-pear",
		"fruit-pear-overlay",
		"fruit-grapes",
		"fruit-grapes-overlay",
		"fruit-apple",
		"fruit-apple-overlay",
		"fruit-orange",
		"fruit-orange-overlay"
		// @formatter:on
	};

	static String[] animatedElements = new String[]{
		// @formatter:off
		"sliderb#",
		"sliderfollowcircle-#",
		"followpoint-#",
		"taikohitcircleoverlay-#",
		"taikobigcircleoverlay-#"
		// @formatter:on
	};

	static List<String> elements = new ArrayList<String>(Arrays.asList(nonAnimatedElements));

	static Map<String, Map<Dimension, Integer>> counts = new HashMap<String, Map<Dimension, Integer>>();

	static ImageReader reader;

	public static void main(String[] args){
		init();
		skinFolders.forEach(folder->{
			try{
				traverseSkinFolder(folder);
			}catch(IOException e){
				e.printStackTrace();
			}
		});
		outputFile();
		System.out.println("done");
	}

	private static void init(){
		// ImageReader
		Iterator<ImageReader> readers = ImageIO.getImageReadersBySuffix("png");
		if(readers.hasNext()){
			reader = readers.next();
		}
		// Folder list
		for(String pathString : inputs){
			skinFolders.add(Paths.get(pathString));
		}
		// Element list
		for(String element : animatedElements){
			// check only the non-animated and -0 versions for animated elements
			String baseElement = new String(element).replaceAll("-#", "").replaceAll("#", "");
			String animatedElement = new String(element).replaceAll("-#", "-0").replaceAll("#", "0");

			elements.add(baseElement);
			elements.add(animatedElement);
		}
		// Counts map
		for(String element : elements){
			System.out.println(element);
			counts.put(element, new HashMap<Dimension, Integer>());
		}
	}

	private static void traverseSkinFolder(Path folder) throws IOException{
		if(Files.exists(folder.resolve("skin.ini"))){
			indexSkinFolder(folder);
		}else{
			Files.list(folder).forEach(file->{
				if(Files.isDirectory(file)){
					// subfolder
					try{
						traverseSkinFolder(file);
					}catch(IOException e){
						e.printStackTrace();
					}
				}else{
					// zip/osk files
					String filename = file.getFileName().toString().toLowerCase(Locale.ROOT);
					if(filename.endsWith(".osk") || filename.endsWith(".zip")){
						indexZipSkin(file);
					}
				}
			});
		}
	}

	private static void indexSkinFolder(Path folder){
		skinCount++;

		for(String element : elements){
			indexElementFromFolder(folder, element);
		}
	}

	private static void indexElementFromFolder(Path folder, String element){
		try{
			Path sdFile = folder.resolve(element + ".png");
			Path hdFile = folder.resolve(element + "@2x.png");
			if(Files.notExists(sdFile) && Files.notExists(hdFile)){ // apparently you cant just flip .exists, we need both this check and the one after...
				return;
			}
			Dimension sdDim = Files.exists(sdFile) ? getImageSize(sdFile) : null;
			Dimension hdDim = Files.exists(hdFile) ? getImageSize(hdFile) : null;

			indexElement(sdDim, hdDim, element);
		}catch(EOFException | IIOException e){
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	private static void indexElementFromZip(ZipFile zip, String element){
		try{
			ZipEntry sdFile = null;
			ZipEntry hdFile = null;
			for(Enumeration<? extends ZipEntry> z = zip.entries(); z.hasMoreElements();){
				ZipEntry entry = z.nextElement();
				String entryname = entry.getName().toLowerCase(Locale.ROOT);
				if(entryname.equals(element + ".png")){
					sdFile = entry;
				}else if(entryname.equals(element + "@2x.png")){
					hdFile = entry;
				}
			}

			Dimension sdDim = sdFile != null ? getZipImageSize(zip, sdFile) : null;
			Dimension hdDim = hdFile != null ? getZipImageSize(zip, hdFile) : null;

			indexElement(sdDim, hdDim, element);
		}catch(IOException e){
			e.printStackTrace();
		}catch(IllegalArgumentException e){
			System.out.println(zip.getName());
			e.printStackTrace();
			throw e;
		}
	}

	private static void indexElement(Dimension sdDim, Dimension hdDim, String element){
		Dimension fileDimension;

		if(sdDim == null && hdDim == null){
			return;
		}

		if(sdDim != null){
			fileDimension = sdDim;
		}else{
			fileDimension = new Dimension(hdDim.width / 2, hdDim.height / 2);
		}

		Map<Dimension, Integer> map = counts.get(element);
		if(map.containsKey(fileDimension)){
			map.put(fileDimension, map.get(fileDimension) + 1);
		}else{
			map.put(fileDimension, 1);
		}
	}

	private static void indexZipSkin(Path file){
		skinCount++;
		try(ZipFile zip = new ZipFile(file.toString(), Charset.forName("ISO-8859-1"))){
			for(String element : elements){
				indexElementFromZip(zip, element);
			}
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	private static Dimension getZipImageSize(ZipFile zip, ZipEntry entry) throws IOException{
		try(ImageInputStream stream = ImageIO.createImageInputStream(zip.getInputStream(entry))){
			return getSize(stream);
		}
	}

	private static Dimension getImageSize(Path file) throws IOException{
		try(FileImageInputStream stream = new FileImageInputStream(file.toFile())){
			return getSize(stream);
		}
	}

	private static Dimension getSize(ImageInputStream stream) throws IOException{
		reader.setInput(stream);
		int index = reader.getMinIndex();
		return new Dimension(reader.getWidth(index), reader.getHeight(index));
	}

	private static void outputFile(){
		try(PrintWriter writer = new PrintWriter("D:\\Downloads\\output.txt")){

			writer.println("Something is considered a skin if it either contains a skin.ini file, or if it is a .zip or .osk file. The sizes are all in SD dimensions, elements in a skin that only had HD versions got their sizes halfed already. ");
			writer.println("Skins checked: " + skinCount);
			writer.println("---");

			counts.forEach((elementKey, elementResultMap)->{
				writer.println("# " + elementKey);
				writer.println("| w x h | count |");
				writer.println("| :-: | :-: |");
				elementResultMap.entrySet();
				new ArrayList<>(elementResultMap.entrySet()).stream().sorted((e1, e2)->{
					Dimension dim1 = e1.getKey();
					Dimension dim2 = e2.getKey();
					if(dim1.width != dim2.width){
						return Integer.compare(dim1.width, dim2.width);
					}else{
						return Integer.compare(dim1.height, dim2.height);
					}
				}).forEach(entry->{
					Dimension dim = entry.getKey();
					writer.println(String.format("| %d x %d | %d |", dim.width, dim.height, entry.getValue()));
				});
			});

			writer.close();
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}
	}
}
